#!/usr/bin/env python3

import configparser
import io
import os 
import re
import sys
from pathlib import Path

# Python current file path
dir_path = Path(os.path.dirname(os.path.realpath(__file__)))

# User configurable values
FILTER_FILE_NAME="filters.txt"

def get_filters():
    filters = []
    
    with open(dir_path/FILTER_FILE_NAME, "r") as filter_file:
        for line in filter_file.readlines():
            filters.append(line.strip().strip("/"))

    return filters

def get_config():
    config_str = sys.stdin.read()
    buf = io.StringIO(config_str)
    dconf_config = configparser.ConfigParser()
    dconf_config.read_file(buf)
    return dconf_config

def filter_config(config, filters):
    out_config = configparser.ConfigParser()
    
    for filter_line in filters:
        filter_line = filter_line.strip()
        # Check to see if it is an existing section
        if config.has_section(filter_line):
            out_config[filter_line] = config[filter_line]
        else:
            # Check to see if maybe it is an exact key
            key = filter_line.split("/")[-1].strip()
            section = filter_line[0:filter_line.rfind(key)].strip("/")
            
            if config.has_option(section, key):
                if not out_config.has_section(section):
                    out_config[section] = {}

                out_config[section][key] = config[section][key]
    
    buf = io.StringIO()
    out_config.write(buf)
    return buf.getvalue().strip()

def main():
    dconf_config = get_config()
    filters = get_filters()

    output = filter_config(dconf_config, filters)
    print(output)

if __name__ == "__main__":
    main()