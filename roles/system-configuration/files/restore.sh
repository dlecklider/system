#!/bin/bash

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

BACKUP_FILE_NAME="settings.conf"

dconf load / < "$SCRIPT_PATH/$BACKUP_FILE_NAME"