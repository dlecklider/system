#!/bin/bash

SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

BACKUP_ALL_FILE_NAME="settings.$HOSTNAME.all.conf"
BACKUP_FILE_NAME="settings.conf"

dconf dump / > "$SCRIPT_PATH/$BACKUP_ALL_FILE_NAME"
dconf dump / | "$SCRIPT_PATH/filter.py" > "$SCRIPT_PATH/$BACKUP_FILE_NAME"