#!/usr/bin/env bash
#
# check_ghidra_update.sh
#
# Checks the local Ghidra version against the latest GitHub release.
# Exit code: 0 if up-to-date or newer, 1 if a newer version is available.
#
# Usage:
#   ./check_ghidra_update.sh /path/to/ghidra
#

set -e

GHIDRA_DIR="$1"
if [ -z "$GHIDRA_DIR" ]; then
  echo "Usage: $0 /path/to/ghidra_install_dir"
  exit 1
fi

if [ ! -d "$GHIDRA_DIR" ]; then
  echo "Error: Directory '$GHIDRA_DIR' does not exist."
  exit 1
fi

################################################################################
# 1) Parse the local Ghidra version
################################################################################
# Typically in Ghidra/<something>.properties with a line like:
#   application.version=10.3
#   application.release.name=PUBLIC
#
# Adjust this grep if your Ghidra folder is structured differently.
################################################################################
LOCAL_VERSION_FILE="$GHIDRA_DIR/Ghidra/application.properties"
if [ ! -f "$LOCAL_VERSION_FILE" ]; then
  echo "Error: Could not find '$LOCAL_VERSION_FILE' to read local version."
  exit 1
fi

LOCAL_VERSION="$(grep -E '^application.version=' "$LOCAL_VERSION_FILE" \
                | cut -d'=' -f2 | tr -d '[:space:]')"

if [ -z "$LOCAL_VERSION" ]; then
  echo "Error: Failed to parse local Ghidra version from $LOCAL_VERSION_FILE."
  exit 1
fi

echo "Local Ghidra version: $LOCAL_VERSION"

################################################################################
# 2) Fetch the latest release tag from GitHub
################################################################################
# We'll call the GitHub API for the latest release:
#   https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest
# The JSON typically has: "tag_name": "Ghidra_10.4_PUBLIC"
#
# We'll parse out the version digits to something like "10.4" or "10.4.3"
################################################################################
LATEST_RELEASE_JSON="$(curl -s https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest)"

if [ -z "$LATEST_RELEASE_JSON" ]; then
  echo "Error: Unable to retrieve GitHub release information."
  exit 1
fi

# Extract the "tag_name" field (e.g. "Ghidra_10.4_PUBLIC")
REMOTE_TAG=$(echo "$LATEST_RELEASE_JSON" \
  | grep -Po '"tag_name":\s*"\K[^"]+' || true)

if [ -z "$REMOTE_TAG" ]; then
  echo "Error: Could not parse remote tag_name from GitHub JSON."
  exit 1
fi

# Remote tag is often "Ghidra_10.4_PUBLIC" or "Ghidra_10.3.2_PUBLIC", etc.
# Let's strip off "Ghidra_" prefix and "_PUBLIC" suffix to leave just the numeric version.
REMOTE_VERSION=$(echo "$REMOTE_TAG" \
  | sed -E 's/^Ghidra_([0-9]+\.[0-9]+(\.[0-9]+)?).*$/\1/' )

if [ -z "$REMOTE_VERSION" ]; then
  echo "Error: Could not normalize remote version from tag '$REMOTE_TAG'."
  exit 1
fi

echo "Latest GitHub version: $REMOTE_VERSION"

################################################################################
# 3) Compare Versions
################################################################################
# We'll do a "sort -V" (version sort) trick to see which is newer.
#
# - If local version is older than remote -> new version -> exit 1
# - If local version is the same or newer -> exit 0
################################################################################

SORTED_VERSIONS="$(printf '%s\n%s\n' "$LOCAL_VERSION" "$REMOTE_VERSION" | sort -V)"
LOWEST="$(echo "$SORTED_VERSIONS" | head -n1)"
HIGHEST="$(echo "$SORTED_VERSIONS" | tail -n1)"

# If both are the same => no new version
if [ "$LOWEST" = "$HIGHEST" ]; then
  echo "Local version == Remote version. You are up to date."
  exit 0
fi

# If local is the lowest and remote is the highest => a new version is available
if [ "$LOWEST" = "$LOCAL_VERSION" ] && [ "$HIGHEST" = "$REMOTE_VERSION" ]; then
  echo "A newer Ghidra version ($REMOTE_VERSION) is available on GitHub."
  exit 1
else
  # Otherwise local is actually higher (possibly a dev build or pre-release).
  echo "Local Ghidra ($LOCAL_VERSION) is newer or same as remote ($REMOTE_VERSION)."
  exit 0
fi
