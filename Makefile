# Makefile
ANSIBLE_CMD = ansible-playbook

provision-local-all:
	@PATH=${PATH}:${HOME}/.local/bin; $(ANSIBLE_CMD) -K local.yml

vscode:
	$(ANSIBLE_CMD) -K local.yml --tags vscode

backup:
	@./roles/system-configuration/files/backup.sh

restore:
	@./roles/system-configuration/files/restore.sh

.PHONY: provision-local-all backup restore