# Purpose

A general purpose system setup and configuration repository to allow for systems to be provisioned and kept up to date simply.

# Usage

`wget -qO- https://dlecklider.gitlab.io/system/bootstrap | bash`

or

`curl https://dlecklider.gitlab.io/system/bootstrap | bash`